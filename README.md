# git 

Project contains scripts to install git.

## Installing

Linux installs: 

```sh
curl -fsS https://gitlab.com/effectualinc/utilities/git/-/raw/master/install.sh | bash
```

wget
```sh
wget https://gitlab.com/effectualinc/utilities/git/-/raw/master/install.sh  -O- | bash
```